const chai = require('chai');
const assert = chai.assert;
const getRelations = require('../lib/relations').getRelations;
const removeNonRelatedFilters = require('../lib/relations').removeNonRelatedFilters;

describe('Relations utils', () => {

	const nameFilter = {
		operator: 'eq',
		field: 'name',
		value: 'test'
	};

	it('should remove non related filters', () => {
		const filters = [
			nameFilter, {
				operator: 'has',
				field: 'nonRelated',
				value: [ nameFilter	]
			}, {
				operator: 'has',
				field: 'related',
				value: [
					nameFilter, 
					{
						operator: 'has',
						field: 'nonRelated2',
						value: [ nameFilter ]
					}, {
						operator: 'has',
						field: 'related2',
						value: [ nameFilter ]
					}
				]
			}
		];

		const relations = [{
			name: 'related',
			relations: [{
				name: 'related2',
				relations: []
			}]
		}];

		const filtersWithRelatesOnly = removeNonRelatedFilters(relations)(filters);

		const expected = [
			nameFilter, {
				operator: 'has',
				field: 'related',
				value: [
					nameFilter, {
						operator: 'has',
						field: 'related2',
						value: [ nameFilter ]
					}
				]
			}
		];

		assert.deepEqual(filtersWithRelatesOnly, expected);
	});

	it('should remove unrelated subfilters of filter without field', () => {
		const filters = [{
			operator: 'or',
			value: [
				nameFilter, {
					operator: 'has',
					field: 'nonRelated',
					value: [ nameFilter ]
				}, {
					operator: 'has',
					field: 'related',
					value: [ nameFilter ]
				}
			]
		}];

		const relations = [{
			name: 'related',
			relations: []
		}];

		const filtersWithRelatesOnly = removeNonRelatedFilters(relations)(filters);

		const expected = [{
			operator: 'or',
			value: [
				nameFilter, {
					operator: 'has',
					field: 'related',
					value: [ nameFilter ]
				}
			]
		}];

		assert.deepEqual(filtersWithRelatesOnly, expected);
	});
});
