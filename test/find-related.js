const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const storageLoader = require('../lib/storageLoader');
const mockAdapter = require('../mocks/mockAdapter');

describe('Find related', () => {

	it('should find entity from related storage', async function () {
		const entityToInsert = {
			title: 'test deep',
			testEntity2: {
				title: 'test deep2'
			}
		};

		const storage = this.getStorage('testEntity');
		const insertedEntity = await storage.insertOne(entityToInsert);

		const foundEntity = await storage.findOneById(insertedEntity.id, [{ name: 'testEntity2' }]);

		assert.deepEqual(foundEntity, insertedEntity);
	});

	it('should find entities from deep storages', async function () {
		const entityToInsert = {
			title: 'test deep',
			testEntityRelated: {
				title: 'test deep2',
				testEntity: {
					title: 'test deep3'
				}
			}
		}

		const storage = this.getStorage('testEntityRelated2');
		const insertedEntity = await storage.insertOne(entityToInsert);

		const foundEntity = await storage.findOneById(insertedEntity.id, [{
			name: 'testEntityRelated',
			relations: [{
				name: 'testEntity'
			}]
		}]);

		assert.deepEqual(foundEntity, insertedEntity);
	});

	it('should find more related entities in array', async function () {
		const entityToInsert = {
			title: 'test deep',
			testEntityRelated: [{
				title: 'deep related1',
				testEntityRelated2: [{
					title: 'deeper related 1'
				}]
			}, {
				title: 'deep related2',
				testEntityRelated2: [{
					title: 'deeper related 2'
				}]
			}]
		};

		const storage = this.getStorage('testEntity');
		await storage.insertOne(entityToInsert);
		const insertedEntity = await storage.insertOne(entityToInsert);

		const foundEntity = await storage.findOneById(insertedEntity.id, [{
			name: 'testEntityRelated',
			relations: [{
				name: 'testEntityRelated2'
			}]
		}]);

		assert.deepEqual(foundEntity, insertedEntity);
	});

	it('should findAll entities with related entity', async function () {
		const entityToInsert = {
			title: 'test deep',
			testEntity2: {
				title: 'test deep2'
			}
		};

		const storage = this.getStorage('testEntity');
		const insertedEntity1 = await storage.insertOne(entityToInsert);
		const insertedEntity2 = await storage.insertOne(entityToInsert);

		const entities = await storage.find({ relations: [{ name: 'testEntity2' }] });

		assert.deepEqual(entities, [insertedEntity1, insertedEntity2]);
	});

	it('should findAll entities with related arrays of entities', async function () {
		const entityToInsert = {
			title: 'test deep',
			testEntityRelated: [{
				title: 'deep related1'
			}, {
				title: 'deep related2'
			}]
		};

		const storage = this.getStorage('testEntity');
		const insertedEntity1 = await storage.insertOne(entityToInsert);
		const insertedEntity2 = await storage.insertOne(entityToInsert);

		const entities = await storage.find({ relations: [{ name: 'testEntityRelated' }] });
		assert.deepEqual(entities, [insertedEntity1, insertedEntity2]);
	});
});