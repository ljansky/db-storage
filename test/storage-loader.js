const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const storageLoader = require('../lib/storageLoader');
const mockAdapter = require('../mocks/mockAdapter');

describe('Storage loader', () => {

    it('Should get storage by getStorage function', async function () {

    	const configs = [{
    		name: 'testStorage',
    		table: 'test_entity',
    		field: [{
    			name: 'id'
    		}]
    	}];

    	const adapter = mockAdapter.createAdapter({});

        const getStorage = await storageLoader.getStorageFactory(configs, [], adapter);
		const storage = getStorage('testStorage');

		assert.isObject(storage);
    });

    it('Should load storageConfigs from given paths and core-path', async function () {
    	const paths = [__dirname + '/../test-models'];
    	const configs = await storageLoader.loadStorageConfigs(paths);

    	const testEntityConfig = R.find(config => config.name === 'testEntity')(configs);
    	assert.isDefined(testEntityConfig);
    });
});
