const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const storageLoader = require('../lib/storageLoader');
const mockAdapter = require('../mocks/mockAdapter');

const pickInsertedData = R.pick(['id', 'title', 'test_number']);

describe('Storage basic functionality', () => {

	it('Should get storage by getStorage function', function () {
		const storage = this.getStorage('testEntity');

		assert.isDefined(storage);
		assert.isFunction(storage.find);
		assert.isFunction(storage.findOneById);
		assert.isFunction(storage.insertOne);
		assert.isFunction(storage.deleteOneById);
		assert.isFunction(storage.updateOneById);
	});

	describe('Basic entity functions', async function () {

		const entityToInsert1 = {
			title: 'test1',
			test_number: 1
		};

		const entityToInsert2 = {
			title: 'test2',
			test_number: 2
		};

		const entityToInsert3 = {
			title: 'test3',
			test_number: 1
		}

		beforeEach(async function () {
			this.storage = this.getStorage('testEntity');
			this.insertedEntity1 = await this.storage.insertOne(entityToInsert1);
			this.insertedEntity2 = await this.storage.insertOne(entityToInsert2);
			this.insertedEntity3 = await this.storage.insertOne(entityToInsert3);

			assert.equal(this.insertedEntity1.title, entityToInsert1.title);
		});

		it('Should findOne created entity', async function () {
			const foundEntity = await this.storage.findOneById(this.insertedEntity1.id);
			assert.deepEqual(foundEntity, this.insertedEntity1);
		});

		it('Should findAll entities', async function () {
			const foundEntities = await this.storage.find();
			assert.deepEqual(foundEntities, [this.insertedEntity1, this.insertedEntity2, this.insertedEntity3]);
		});

		it('Should updateOne created entity', async function () {

			const expected = Object.assign({}, this.insertedEntity1, { title: 'updated1' });

			const nothing = await this.storage.updateOneById('non-existing-id', { title: expected.title });
			assert.isNull(nothing);

			const updatedEntity = await this.storage.updateOneById(this.insertedEntity1.id, { title: expected.title });
			assert.deepEqual(updatedEntity, expected);

			const foundEntity = await this.storage.findOneById(this.insertedEntity1.id);
			assert.deepEqual(foundEntity, expected);
		});

		it('Should deleteOne created entity', async function () {
			const nothing = await this.storage.deleteOneById('non-existing-id');
			assert.isNull(nothing);

			const deletedEntity = await this.storage.deleteOneById(this.insertedEntity1.id);
			assert.deepEqual(deletedEntity, this.insertedEntity1);

			const foundEntity = await this.storage.findOneById(this.insertedEntity1.id);
			assert.isNull(foundEntity);
		});

		it('should be possible to limit entities', async function () {
			const data = await this.storage.find({ limit: 2 });
			assert.deepEqual(data, [this.insertedEntity1, this.insertedEntity2]);
		});

		it('should be possible to find entities with offset', async function () {
			const data = await this.storage.find({ offset: 1 });
			assert.deepEqual(data, [this.insertedEntity2, this.insertedEntity3]);
		});

		it('should find entities with limit and offset', async function () {
			const data = await this.storage.find({ offset: 1, limit: 1 });
			assert.deepEqual(data, [this.insertedEntity2]);
		});
	});
});
