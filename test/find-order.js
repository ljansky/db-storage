const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const storageLoader = require('../lib/storageLoader');
const mockAdapter = require('../mocks/mockAdapter');

describe('Storage find order', () => {

	const entityToInsert1 = {
		title: 'test1',
		test_number: 1
	};

	const entityToInsert2 = {
		title: 'test2',
		test_number: 2
	};

	const entityToInsert3 = {
		title: 'test3',
		test_number: 1
	}

	beforeEach(async function () {
		this.storage = this.getStorage('testEntity');
		this.insertedEntity1 = await this.storage.insertOne(entityToInsert1);
		this.insertedEntity2 = await this.storage.insertOne(entityToInsert2);
		this.insertedEntity3 = await this.storage.insertOne(entityToInsert3);
	});

	it('should order entities by id desc', async function () {
		const order = [{
			by: 'id',
			desc: true
		}];

		const data = await this.storage.find({ order });
		assert.deepEqual(data, [this.insertedEntity3, this.insertedEntity2, this.insertedEntity1]);
	});

	it('should order and limit and offset', async function () {
		const order = [{
			by: 'id',
			desc: true
		}];

		const data = await this.storage.find({ order, limit: 1, offset: 2 });
		assert.deepEqual(data, [this.insertedEntity1]);
	});

	it('should order by more parameters', async function () {
		const order = [{
			by: 'test_number'
		}, {
			by: 'id',
			desc: true
		}];

		const data = await this.storage.find({ order });
		assert.deepEqual(data, [this.insertedEntity3, this.insertedEntity1, this.insertedEntity2]);
	});
});
