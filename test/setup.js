const dbTestConnection = require('./dbTestConnection');

before(async function () {
	await dbTestConnection.init(this);
});

beforeEach(async function () {
	await dbTestConnection.start(this);
});

afterEach (async function () {
	await dbTestConnection.stop(this);
});