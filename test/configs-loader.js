const chai = require('chai');
const assert = chai.assert;
const configsLoader = require('../lib/configsLoader');
const R = require('ramda');

describe('Configs loader', () => {
    it('Should load configs from given paths', async function () {
    	const paths = [__dirname + '/../test-models'];
    	const configs = await configsLoader.loadXmlConfigsFromPaths(paths);

    	const testEntityConfig = R.find(config => config.model.$.name === 'testEntity')(configs);

    	assert.isDefined(testEntityConfig);
    });
});
