const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const storageLoader = require('../lib/storageLoader');
const mockAdapter = require('../mocks/mockAdapter');

describe('FindAll filter functions', () => {

	const entitiesToInsert = [{
		title: 'teest1',
		test_number: 2
	}, {
		title: 'test2',
		test_number: 1
	}, {
		title: 'teest3',
		test_number: 4
	}, {
		title: 'test4',
		test_number: 3
	}];

	beforeEach(async function () {
    	this.storage = this.getStorage('testEntity');
		this.insertedEntities = [];
		for (entity of entitiesToInsert) {
			const insertedEntity = await this.storage.insertOne(entity);
			this.insertedEntities.push(insertedEntity);
		}
	});

	it('Should be possible to find entities filtered with operator', async function () {
		const filter = [{
			operator: 'gt',
			field: 'test_number',
			value: 2
		}];

		const foundEntities = await this.storage.find({ filter });
		assert.deepEqual(foundEntities, [this.insertedEntities[2], this.insertedEntities[3]]);
	});

	it('should be possible to find entities filtered with like operator', async function () {
		const filter = [{
			operator: 'like',
			field: 'title',
			value: 'ee'
		}];

		const foundEntities = await this.storage.find({ filter });
		assert.deepEqual(foundEntities, [this.insertedEntities[0], this.insertedEntities[2]]);
	});

	it('should be possible to find entities filtered with more operators', async function () {
		const filter = [{
			operator: 'gt',
			field: 'test_number',
			value: 1
		}, {
			operator: 'lte',
			field: 'test_number',
			value: 3
		}];

		const foundEntities = await this.storage.find({ filter });
		assert.deepEqual(foundEntities, [this.insertedEntities[0], this.insertedEntities[3]]);
	});

	it('should be possible to use and operator', async function () {
		const filter = [{
			operator: 'and',
			value: [{
				operator: 'gte',
				field: 'test_number',
				value: 2
			}, {
				operator: 'lte',
				field: 'test_number',
				value: 3
			}]
		}];

		const foundEntities = await this.storage.find({ filter });
		assert.deepEqual(foundEntities, [this.insertedEntities[0], this.insertedEntities[3]]);
	});

	it('should be possible to use or operator', async function () {
		const filter = [{
			operator: 'or',
			field: '',
			value: [{
				operator: 'lt',
				field: 'test_number',
				value: 2
			}, {
				operator: 'gt',
				field: 'test_number',
				value: 3
			}]
		}];

		const foundEntities = await this.storage.find({ filter });
		assert.deepEqual(foundEntities, [this.insertedEntities[1], this.insertedEntities[2]]);
	});

	describe('Filtering with has operator', () => {

		const formatEntitiesForAssert = R.map(entity => ({
			id: entity.id,
			title: entity.title,
			test_number: entity.test_number,
			testEntityRelated: entity.testEntityRelated.map(rel => ({
				id: rel.id,
				title: rel.title,
				test_entity_id: rel.test_entity_id
			}))
		}));

		beforeEach(async function () {
			const entitiesWithRelatesToInsert = [{
				title: 'withRelated1',
				test_number: 5,
				testEntityRelated: [{
					title: 'related1',
					testEntityRelated2: []
				}, {
					title: 'related2',
					testEntityRelated2: [{
						title: 'deep1'
					}]
				}]
			}, {
				title: 'withRelated2',
				test_number: 7,
				testEntityRelated: [{
					title: 'related1',
					testEntityRelated2: [{
						title: 'deep2'
					}]
				}, {
					title: 'related3'
				}]
			}, {
				title: 'withRelated3',
				test_number: 6,
				testEntityRelated: [{
					title: 'related2',
					testEntityRelated2: [{
						title: 'deep1'
					}]
				}]
			}];

			this.insertedEntitiesWithRelates = [];

			for (entity of entitiesWithRelatesToInsert) {
				const insertedEntity = await this.storage.insertOne(entity);
				this.insertedEntitiesWithRelates.push(insertedEntity);
			}
		});

		it('should be possible to find entities containing related entities of given parameters', async function () {
			const relations = [{ name: 'testEntityRelated' }];

			const filter = [{
				operator: 'has',
				field: 'testEntityRelated',
				value: [{
					operator: 'eq',
					field: 'title',
					value: 'related2'
				}]
			}];

			const foundEntities = await this.storage.find({ filter, relations });
			assert.deepEqual(formatEntitiesForAssert(foundEntities), formatEntitiesForAssert([this.insertedEntitiesWithRelates[0], this.insertedEntitiesWithRelates[2]]));
		});

		it('should be possible to find entities containing related entities filtered with more filters', async function () {
			const relations = [{ name: 'testEntityRelated' }];

			const filter = [{
				operator: 'has',
				field: 'testEntityRelated',
				value: [{
					operator: 'in',
					field: 'test_entity_id',
					value: [this.insertedEntitiesWithRelates[1].id, this.insertedEntitiesWithRelates[2].id]
				}, {
					operator: 'eq',
					field: 'title',
					value: 'related2'
				}]
			}];

			const foundEntities = await this.storage.find({ filter, relations });
			assert.deepEqual(formatEntitiesForAssert(foundEntities), formatEntitiesForAssert([this.insertedEntitiesWithRelates[2]]));
		});

		it('should be possible to find entities filtered by deep related entities', async function () {
			const relations = [{
				name: 'testEntityRelated',
				relations: [{
					name: 'testEntityRelated2'
				}]
			}];

			const filter = [{
				operator: 'has',
				field: 'testEntityRelated',
				value: [{
					operator: 'has',
					field: 'testEntityRelated2',
					value: [{
						operator: 'in',
						field: 'title',
						value: ['deep1']
					}]
				}]
			}];

			const foundEntities = await this.storage.find({ filter, relations });
			assert.deepEqual(foundEntities, [this.insertedEntitiesWithRelates[0], this.insertedEntitiesWithRelates[2]]);
		});
	});
});