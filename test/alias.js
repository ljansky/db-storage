const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');

describe('Alias', () => {

	it('Should be possible to create entity with alias', async function () {
		const entityToInsert = {
			title: 'test deep',
			alias: {
				title: 'test alias'
			}
		};

		const storage = this.getStorage('testEntity');
		const insertedEntity = await storage.insertOne(entityToInsert);

		assert.isDefined(insertedEntity.id);
		assert.isDefined(insertedEntity.alias_id);
		assert.equal(insertedEntity.alias_id, insertedEntity.alias.id);

		const foundEntity = await storage.findOneById(insertedEntity.id);
		assert.isUndefined(foundEntity.alias);

		const relatedStorage = this.getStorage('testEntity2');
		const foundRelatedEntity = await relatedStorage.findOneById(insertedEntity.alias_id);
		assert.isNotNull(foundRelatedEntity);
	});
});