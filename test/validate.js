const chai = require('chai');
const assert = chai.assert;
const storageLoader = require('../lib/storageLoader');
const mockAdapter = require('../mocks/mockAdapter');
const validate = require('../mocks/mockValidator');

describe('Storage validate', () => {

	const storageConfigs = [{
		name: 'test',
		table: 'test',
		field: [{
			name: 'withoutRule'
		}, {
			name: 'withRule',
			rule: [{ type: 'required' }]
		}]
	}];

	beforeEach(async () => {
		const adapter = mockAdapter.createAdapter({});
		this.getStorage = await storageLoader.getStorageFactory(storageConfigs, [], adapter, validate);
	});

	it('Should validate data and find error', () => {
		const storage = this.getStorage('test');
		const errors = storage.validate({});

		assert.equal(errors.length, 1);
	});

	it('Should validate data without error', () => {
		const storage = this.getStorage('test');
		const errors = storage.validate({
			withRule: 'test'
		});

		assert.equal(errors.length, 0);
	});
});
