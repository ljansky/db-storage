const R = require('ramda');
const error = require('./error');
const getRelations = require('./relations').getRelations;
const removeNonRelatedFilters = require('./relations').removeNonRelatedFilters;


const find = (adapterMethods, storageConfig, getStorage) => async (params = {}) => {

	const relations = getRelations(params.relations || [], getStorage, storageConfig);
	const filter = removeNonRelatedFilters(relations)(params.filter || []);
	const limit = params.limit || 0;
	const offset = params.offset || 0;
	const order = params.order || [];
	const entities = await adapterMethods.find({ filter, limit, offset, order, relations });

	return entities;
};

const findOneById = (adapterMethods, storageConfig, getStorage) => async (id, relations = []) => {
	const filter = [{
		field: 'id',
		value: id,
		operator: 'eq'
	}];

	const entities = await find(adapterMethods, storageConfig, getStorage)({ filter, relations });
	return entities[0] || null;
}

const insertOne = (adapterMethods, storageConfig, getStorage) => async (data) => {
	const relatedStorages = storageConfig.relatedStorages;
	const dataToMergeBefore = {};
	const dataToMergeAfter = {};

	for (let key in data) {
		if (relatedStorages[key]) {
			const relatedStorage = getStorage(relatedStorages[key].foreign);
			const insertedSubEntity = await relatedStorage.insertOne(data[key]);
			dataToMergeBefore[relatedStorages[key].name] = insertedSubEntity.id;
			dataToMergeAfter[key] = insertedSubEntity;
		} else {
			const relatedStorage = getStorage(key);
			if (relatedStorage && relatedStorage.storageConfig.relatedStorages[storageConfig.name]) {
				dataToMergeAfter[key] = [];
			}
		}
	}

	const mergedData = R.pipe(
		R.omit(R.keys(dataToMergeAfter)),
		R.merge(R.__, dataToMergeBefore)
	)(data);

	const insertedEntity = await adapterMethods.insertOne(mergedData);

	for (let key in data) {
		const relatedStorage = getStorage(key);
		if (relatedStorage && relatedStorage.storageConfig.relatedStorages[storageConfig.name]) {
			const relatedFieldName = relatedStorage.storageConfig.relatedStorages[storageConfig.name].name;
			const insertedSubEntities = [];
			for (let relatedData of data[key]) {
				const insertedSubEntity = await relatedStorage.insertOne(R.assoc(relatedFieldName, insertedEntity.id, relatedData));
				insertedSubEntities.push(insertedSubEntity);
			}

			dataToMergeAfter[key] = insertedSubEntities;
		}
	}

	return R.merge(insertedEntity, dataToMergeAfter);
};

const updateOneById = (adapterMethods, storageConfig, getStorage) => async (id, data) => {
	const relatedStorages = storageConfig.relatedStorages;
	const entity = await findOneById(adapterMethods, storageConfig, getStorage)(id);

	if (!entity) {
		return null;
	}

	const dataToMergeBefore = {};
	const dataToMergeAfter = {};

	for (let key in data) {
		if (relatedStorages[key]) {
			const relatedStorage = getStorage(relatedStorages[key].foreign);
			let subEntity = null;
			if (typeof entity[relatedStorages[key].name] !== 'undefined' && entity[relatedStorages[key].name] !== null) {
				subEntity = await relatedStorage.updateOneById(entity[relatedStorages[key].name], data[key]);
			} else {
				subEntity = await relatedStorage.insertOne(data[key]);
			}

			if (subEntity) {
				dataToMergeBefore[relatedStorages[key].name] = subEntity.id;
				dataToMergeAfter[key] = subEntity;
			}
		} else {
			const relatedStorage = getStorage(key);
			if (relatedStorage && relatedStorage.storageConfig.relatedStorages[storageConfig.name]) {
				dataToMergeAfter[key] = [];
			}
		}
	}

	const mergedData = R.pipe(
		R.omit(R.keys(dataToMergeAfter)),
		R.merge(R.__, dataToMergeBefore)
	)(data);

	const updatedEntity = await adapterMethods.updateOneById(id, mergedData);
	return R.merge(updatedEntity, dataToMergeAfter);
};

const deleteOneById = adapterMethods => id => {
	return adapterMethods.deleteOneById(id);
};

const validate = (fields, validateObject) => entity => {
	const rules = [];

	fields.forEach(field => {
		if (field.rule) {
			field.rule.forEach(rule => {
				rules.push({
					field: field.name,
					type: rule.type
				});
			});
		}
	});

	const errors = validateObject(entity, rules);

	return R.pipe(
		R.toPairs,
	)(errors);
}

const createStorage = (config, adapter, getStorage, validateObject) => {
	const storageTable = config.table;
	if (!storageTable) {
		error.throwError(500, 'Model without table.');
	}

	const relatedStorages = R.pipe(
		R.filter(field => field.foreign),
		R.reduce((acc, curr) => R.merge(acc, { [curr.as || curr.foreign]: curr }) , {})
	)(config.field);

	const adapterMethods = adapter(storageTable);

	const storageConfig = {
		name: config.name,
		table: config.table,
		relatedStorages
	};

	return {
		find: find(adapterMethods, storageConfig, getStorage),
		findOneById: findOneById(adapterMethods, storageConfig, getStorage),
		insertOne: insertOne(adapterMethods, storageConfig, getStorage),
		updateOneById: updateOneById(adapterMethods, storageConfig, getStorage),
		deleteOneById: deleteOneById(adapterMethods),
		validate: validate(config.field, validateObject),
		storageConfig
	};
};

module.exports = {
	createStorage
};