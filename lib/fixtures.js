const R = require('ramda');
const loadXmlConfigsFromPaths = require('./configsLoader').loadXmlConfigsFromPaths;

const upsertStorageFixture = getStorage => storageName => {

	const storage = getStorage(storageName);
	return async (fixture) => {
		const filter = fixture.id ? [{
			operator: 'eq',
			field: 'id',
			value: fixture.id
		}] : R.pipe(
			R.toPairs,
			R.map(item => ({
				operator: 'eq',
				field: item[0],
				value: item[1]
			}))
		)(fixture);

		const foundFixture = await storage.find({ filter });
		
		if (!foundFixture[0]) {
			const upsertedFixture = await storage.insertOne(fixture);
			return upsertedFixture;
		} else {
			if (fixture.id) {
				const upsertedFixture = await storage.updateOneById(fixture.id, fixture);
			} else {
				return foundFixture[0];
			}
		}
	}
}

const upsertStorageFixtures = getStorage => storageName => async (fixtures) => {

	const upsertFixture = upsertStorageFixture(getStorage)(storageName);

	const upsertedFixtures = [];

	for (let fixture of fixtures) {
		const upserted = await upsertFixture(fixture);
		upsertedFixtures.push(upserted);
	}

	return upsertedFixtures;
};

const getStorageFixtures = R.pipe(
	R.map(fix => {
		const a = R.omit(['$'], R.merge(fix, fix.$));
		return R.mapObjIndexed((val, key, obj) => {
			if (R.type(val) === 'Array') {
				return getStorageFixtures(val);
			} else {
				return val;
			}
		}, a);
	})
);

const upsertFixtures = getStorage => async (fixturesFromFiles) => {
	const fixtures = R.map(R.prop('fixtures'))(fixturesFromFiles);
	for (let fixturesInfo of fixtures) {
		for (let storageName in fixturesInfo) {
			const storageFixtures = getStorageFixtures(fixturesInfo[storageName]);
			await upsertStorageFixtures(getStorage)(storageName)(storageFixtures);
		}
	}
}

module.exports = (getStorage) => {
	return {
		upsertStorageFixtures: upsertStorageFixtures(getStorage),
		upsertFixtures: upsertFixtures(getStorage)
	};
};