const fs = require('fs');
const parseString = require('xml2js').parseString;
const R = require('ramda');

const getFilesFromPath = path => {
	return fs.readdirSync(path)
		.map(fileName => fs.readFileSync(path + '/' + fileName, 'utf8'));
};

const getConfigPromise = content => {
	return new Promise((resolve, reject) => {
		parseString(content, (err, res) => {
			if (!err) {
				resolve(res);
			} else {
				reject(err);
			}
		});
	});
};

const loadXmlConfigsFromPaths = R.pipe(
	paths => R.map(getFilesFromPath, paths),
	filesByPath => R.reduce(R.concat, [], filesByPath),
	allFiles => R.map(getConfigPromise, allFiles),
	promisesToAllConfigs => Promise.all(promisesToAllConfigs)
);

module.exports = {
	loadXmlConfigsFromPaths
};