const R = require('ramda');
const loadXmlConfigsFromPaths = require('./configsLoader').loadXmlConfigsFromPaths;
const createStorage = require('./storage').createStorage;
const fixturesFactory = require('./fixtures');

const getStorageFactory = async (configs, fixtures, adapter, validate) => {
	
	const getStorage = storageName => {
		return storages[storageName] || null;
	};

	const storages = R.pipe(
		R.reduce((acc, curr) => Object.assign({}, acc, {[curr.name]: curr}), {}),
		R.map(config => createStorage(config, adapter, getStorage, validate))
	)(configs);

	const fixturesService = fixturesFactory(getStorage);
	await fixturesService.upsertFixtures(fixtures);

	return getStorage;
};

const formatConfig = configFromXml => {
	return R.merge(configFromXml.model.$, {
		field: configFromXml.model.field ? configFromXml.model.field.map(f => f.$) : []
	});
};

const loadStorageConfigs = R.pipe(
	paths => loadXmlConfigsFromPaths(paths).then(R.map(formatConfig))
);

module.exports = {
	getStorageFactory,
	loadStorageConfigs
};