const R = require('ramda');

const getRelations = (relations, getStorage, storageConfig) => {
	const relatedStorages = storageConfig.relatedStorages;

	return R.pipe(
		R.filter(relation => {
			const hasMany = !relatedStorages[relation.name];
			if (hasMany) {
				const relatedStorage = getStorage(relation.name);
				return relatedStorage.storageConfig.relatedStorages[storageConfig.name];
			} else {
				return true;
			}
		}),
		R.map(relation => {
			const hasMany = !relatedStorages[relation.name];
			const relatedStorage = hasMany ? getStorage(relation.name) : getStorage(relatedStorages[relation.name].foreign);

			return {
				name: relatedStorage.storageConfig.name,
				table: relatedStorage.storageConfig.table,
				ownField: hasMany ? 'id' : relatedStorages[relation.name].name,
				foreignField: hasMany ? relatedStorage.storageConfig.relatedStorages[storageConfig.name].name : 'id',
				hasMany,
				relations: relation.relations ? getRelations(relation.relations, getStorage, relatedStorage.storageConfig) : []
			};
		})
	)(relations);
};

const isWithSubfilter = filter => R.contains(filter.operator, ['has', 'and', 'or']);
const isRelatedFilter = filter => filter.field && isWithSubfilter(filter);

const removeNonRelatedFilters = relations => R.pipe(
	R.filter(f => {
		if (f.field && isWithSubfilter(f)) {
			const hasRelated = R.any(item => item.name === f.field, relations);
			return hasRelated;
		} else {
			return true;
		}
	}),
	R.map(f => {
		if (isWithSubfilter(f)) {
			let subRelations = relations;
			if (f.field) {
				const relation = R.find(item => item.name === f.field, relations);
				subRelations = relation.relations;
			}

			return R.evolve({
				value: removeNonRelatedFilters(subRelations)
			}, f);
		} else {
			return f;
		}
	}),
	R.filter(f => !isWithSubfilter(f) || f.value.length > 0)
);

module.exports = {
	getRelations,
	removeNonRelatedFilters
};