const getStorageFactory = require('./lib/storageLoader').getStorageFactory;
const loadStorageConfigs = require('./lib/storageLoader').loadStorageConfigs;
const mockAdapter = require('./mocks/mockAdapter');
const fixturesFactory = require('./lib/fixtures');
const loadXmlConfigsFromPaths = require('./lib/configsLoader').loadXmlConfigsFromPaths;

module.exports = {
	getStorageFactory,
	loadStorageConfigs,
	mockAdapter,
	fixturesFactory,
	loadXmlConfigsFromPaths
};